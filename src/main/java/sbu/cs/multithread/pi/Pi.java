package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Pi {

    private static BigDecimal pi = new BigDecimal(0).setScale(1000, RoundingMode.HALF_DOWN);

    synchronized public static void addPi(BigDecimal bd) {

        pi = pi.add(bd);
        //System.out.println(pi);
    }

    public static String getPi() { return pi.toString();}


}

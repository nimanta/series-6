package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;


public class Calculator implements Runnable {

    private final int counter;
    private static final BigDecimal ONE = new BigDecimal(1, new MathContext(100, RoundingMode.HALF_DOWN));
    private static final BigDecimal TWO = new BigDecimal(2, new MathContext(100, RoundingMode.HALF_DOWN));
    private static final BigDecimal FOUR = new BigDecimal(4, new MathContext(100, RoundingMode.HALF_DOWN));
    private static final BigDecimal FIVE = new BigDecimal(5, new MathContext(100, RoundingMode.HALF_DOWN));
    private static final BigDecimal SIX = new BigDecimal(6, new MathContext(100, RoundingMode.HALF_DOWN));


    public Calculator(int counter) {

        this.counter = counter;
    }

    @Override
    public void run() {
        BigDecimal multiply = ((new BigDecimal(8))).multiply(new BigDecimal(counter));

        BigDecimal bigDecimal1 = ONE.divide(((new BigDecimal(16))).pow(counter), 1000, RoundingMode.HALF_DOWN);
        BigDecimal bigDecimal2 = FOUR.divide((multiply.add(ONE)), 1000, RoundingMode.HALF_DOWN);
        BigDecimal bigDecimal3 = TWO.divide((multiply.add(FOUR)), 1000, RoundingMode.HALF_DOWN);
        BigDecimal bigDecimal4 = ONE.divide((multiply.add(FIVE)), 1000, RoundingMode.HALF_DOWN);
        BigDecimal bigDecimal5 = ONE.divide((multiply.add(SIX)), 1000, RoundingMode.HALF_DOWN);

        BigDecimal sigmaElement = bigDecimal1.multiply(bigDecimal2.subtract(bigDecimal3).subtract(bigDecimal4).subtract(bigDecimal5));

        Pi.addPi(sigmaElement);
    }
}

package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Source {

    static Semaphore sem = new Semaphore(2);


    public static void getSource(int i, String threadName) {

        System.out.println(threadName + " from " +  i + " is waiting...");
        try {
            Thread.sleep(500);
            sem.acquire();

            System.out.println(threadName + " has access now.");
            Thread.sleep(500);
            System.out.println(threadName + "'s job is done.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        sem.release();
    }
}
